/*
 * Copyright (C) BABEC. All rights reserved.
 * Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 */

package leveldbprovider

import (
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMemdbHandle_Close(t *testing.T) {
	dbHandle := NewMemdbHandle()
	err := dbHandle.Close()
	assert.Nil(t, err)
}

func TestMemdbHandle_CompactRange(t *testing.T) {
	dbHandle := NewMemdbHandle()
	defer dbHandle.Close()
	err := dbHandle.CompactRange([]byte(""), []byte(""))
	assert.Nil(t, err)
}

func TestMemdbHandle_NewIteratorWithPrefix(t *testing.T) {
	dbHandle := NewMemdbHandle()
	defer dbHandle.Close()

	batch := NewUpdateBatch()

	batch.Put([]byte("key1"), []byte("value1"))
	batch.Put([]byte("key2"), []byte("value2"))
	batch.Put([]byte("key3"), []byte("value3"))
	batch.Put([]byte("key4"), []byte("value4"))
	batch.Put([]byte("keyx"), []byte("value5"))

	err := dbHandle.WriteBatch(batch, true)
	assert.Equal(t, nil, err)

	iter, err := dbHandle.NewIteratorWithPrefix([]byte("key"))
	assert.Nil(t, err)
	defer iter.Release()
	var count int
	for iter.Next() {
		count++
	}
	assert.Equal(t, 5, count)

	_, err = dbHandle.NewIteratorWithPrefix([]byte(""))
	assert.NotNil(t, err)
	assert.Equal(t, strings.Contains(err.Error(), "iterator prefix should not be empty key"), true)
}

func TestMemdbHandle_NewIteratorWithRange(t *testing.T) {
	dbHandle := NewMemdbHandle()
	defer dbHandle.Close()

	batch := NewUpdateBatch()
	key1 := []byte("key1")
	value1 := []byte("value1")
	key2 := []byte("key2")
	value2 := []byte("value2")
	batch.Put(key1, value1)
	batch.Put(key2, value2)
	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	iter, err := dbHandle.NewIteratorWithRange(key1, []byte("key3"))
	assert.Nil(t, err)
	defer iter.Release()
	var count int
	for iter.Next() {
		count++
	}
	assert.Equal(t, 2, count)

	_, err = dbHandle.NewIteratorWithRange([]byte(""), []byte(""))
	assert.NotNil(t, err)
	assert.Equal(t, strings.Contains(err.Error(), "iterator range should not start"), true)
}

func TestMemdbHandle_Put(t *testing.T) {
	dbHandle := NewMemdbHandle()
	defer dbHandle.Close()
	key1 := []byte("key1")
	value1 := []byte("value1")
	err := dbHandle.Put(key1, value1)
	assert.Nil(t, err)

	value, err := dbHandle.Get(key1)
	assert.Nil(t, err)
	assert.Equal(t, value1, value)

	exist, err := dbHandle.Has(key1)
	assert.True(t, exist)
	assert.Nil(t, err)
	err = dbHandle.Delete(key1)
	assert.Nil(t, err)

	exist, err = dbHandle.Has(key1)
	assert.False(t, exist)
	assert.Nil(t, err)
}

func TestMemdbHandle_Gets(t *testing.T) {
	dbHandle := NewMemdbHandle()
	defer dbHandle.Close()

	n := 3
	keys := make([][]byte, 0, n+1)
	values := make([][]byte, 0, n+1)
	batch := NewUpdateBatch()
	for i := 0; i < n; i++ {
		keyi := []byte(fmt.Sprintf("key%d", i))
		valuei := []byte(fmt.Sprintf("value%d", i))
		keys = append(keys, keyi)
		values = append(values, valuei)

		batch.Put(keyi, valuei)
	}

	err := dbHandle.WriteBatch(batch, true)
	assert.Nil(t, err)

	keys = append(keys, nil)
	values = append(values, nil)
	valuesR, err1 := dbHandle.GetKeys(keys)
	assert.Nil(t, err1)
	for i := 0; i < len(keys); i++ {
		assert.Equal(t, values[i], valuesR[i])
	}
}
