module chainmaker.org/chainmaker/store-leveldb/v2

go 1.16

require (
	chainmaker.org/chainmaker/common/v2 v2.3.4
	chainmaker.org/chainmaker/protocol/v2 v2.3.5
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0
	github.com/syndtr/goleveldb v1.0.1-0.20200815110645-5c35d600f0ca
)

replace github.com/syndtr/goleveldb => chainmaker.org/third_party/goleveldb v1.1.0
